﻿// Copyright (c) 2017 Fred Morales <guru_meditation@rocketmail.com>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of mosquitto nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace KeyCuts
{
    public static class Shortcuts
    {
        public static RoutedCommand CustomRoutedCommand = new RoutedCommand();

        public static List<T> GetLogicalChildCollection<T>(object parent) where T : DependencyObject
        {
            List<T> logicalCollection = new List<T>();
            GetLogicalChildCollection(parent as DependencyObject, logicalCollection);
            return logicalCollection;
        }

        private static void GetLogicalChildCollection<T>(DependencyObject parent, List<T> logicalCollection) where T : DependencyObject
        {
            IEnumerable children = LogicalTreeHelper.GetChildren(parent);
            foreach (object child in children)
            {
                if (child is DependencyObject)
                {
                    DependencyObject depChild = child as DependencyObject;
                    if (child is T)
                    {
                        logicalCollection.Add(child as T);
                    }
                    GetLogicalChildCollection(depChild, logicalCollection);
                }
            }
        }

        private static Tuple<Key, ModifierKeys> Interpret(string inputGesture)
        {
            ModifierKeys modifierKeys = ModifierKeys.None;

            inputGesture = inputGesture.Replace(" ", "").ToLower();

            if (inputGesture.Contains("ctrl"))
            {
                modifierKeys = ModifierKeys.Control;
            }
            else if (inputGesture.Contains("alt"))
            {
                modifierKeys = ModifierKeys.Alt;
            }
            else if (inputGesture.Contains("shift"))
            {
                modifierKeys = ModifierKeys.Shift;
            }
            else if (inputGesture.Contains("windows"))
            {
                modifierKeys = ModifierKeys.Windows;
            }

            inputGesture = inputGesture.Replace("ctrl", "").ToLower();
            inputGesture = inputGesture.Replace("alt", "").ToLower();
            inputGesture = inputGesture.Replace("shift", "").ToLower();
            inputGesture = inputGesture.Replace("windows", "").ToLower();
            inputGesture = inputGesture.Replace("+", "").ToLower();

            if (inputGesture.Length == 1)
            {
                return new Tuple<Key, ModifierKeys>(Key.A + (inputGesture[0] - 'a'), modifierKeys);
            }
            else if (inputGesture.Length == 6)
            {
                if (inputGesture == "delete")
                {
                    return new Tuple<Key, ModifierKeys>(Key.Delete, modifierKeys);
                }
            }

            return null;
        }

        public static void Init(Window dialog)
        {
            foreach (var item in GetLogicalChildCollection<MenuItem>(dialog))
            {
                Tuple<Key, ModifierKeys> k = Interpret(item.InputGestureText);

                if (k != null)
                {
                    Command command = new Command(item);
                    dialog.CommandBindings.Add(new CommandBinding(command));
                    dialog.InputBindings.Add(new KeyBinding(command, k.Item1, k.Item2));
                }
            }
        }
    }
}
